# SpringBoot整合Mybatis-Plus

#### 介绍
SpringBoot 整合 MyBatisPlus

#### 注解
@TableName
描述：表名注解，标识实体类对应的表
使用位置：实体类

@TableId
描述：主键注解
使用位置：实体类主键字段

@TableField 
描述：字段注解（非主键）

@OrderBy 
描述：内置 SQL 默认指定排序，优先级低于 wrapper 条件查询

#### 主键生成
AUTO 数据库ID自增  
NONE 该类型为未设置主键类型  
INPUT 用户输入ID  
ASSIGN_ID 自动生成（雪花算法）  
ASSIGN_UUID UUID    

#### Swagger3
localhost:8080/swagger-ui/index.html

#### 版本
V1.0.0