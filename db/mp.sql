-- 创建用户表
DROP TABLE IF EXISTS `mp`.`user`;
CREATE TABLE `mp`.`user` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` VARCHAR(30) DEFAULT NULL COMMENT '姓名',
    `age` INT(10) DEFAULT NULL COMMENT '年龄',
    `email` VARCHAR(50) DEFAULT NULL COMMENT '邮箱',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `creator` VARCHAR(32) NOT NULL DEFAULT 'system' COMMENT '创建人',
    `modify_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    `modifier` VARCHAR(32) NOT NULL DEFAULT 'system' COMMENT '修改人',
    `is_deleted` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'N正常-Y删除',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='用户表';

-- 测试数据
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("官燕舞", "16", "guanyanwu@sun.com");
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("原涵菱", "17", "yuanlinhan@sun.com");
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("侨雪晴", "18", "qiaoxueqing@sun.com");
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("洋小宸", "16", "yangxiaochen@sun.com");
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("唐雨信", "17", "tangyuxin@sun.com");
INSERT INTO `mp`.`user` (`name`, `age`, `email`) VALUES ("司寇心", "18", "sikouxin@sun.com");


