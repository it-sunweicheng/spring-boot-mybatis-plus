package com.sun;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@ApiIgnore
@SpringBootApplication
@MapperScan("com.sun.mapper")
public class MybatisPlusApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("Application Startup Success!");
    }

}
