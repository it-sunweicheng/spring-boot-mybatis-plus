package com.sun.aspect;

import java.lang.annotation.*;

/**
 * log自定义注解
 * 
 * @author sun
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Documented
public @interface WebLog {

    String value() default "";

}
