package com.sun.controller;

import com.sun.aspect.WebLog;
import com.sun.common.base.Result;
import com.sun.dto.UserDTO;
import com.sun.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author sun
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Api(value = "用户接口", tags = { "用户接口" })
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @WebLog("新增用户")
    @ApiOperation("新增用户")
    @PostMapping("addUser")
    public Result<Void> addUser(@RequestBody UserDTO user) {
        userService.insertUser(user);
        return Result.success();
    }

    @WebLog("删除用户")
    @ApiOperation("删除用户")
    @GetMapping("deleteUser")
    public Result<Void> deleteUser(@RequestParam("id") Long id) {
        userService.deleteUser(id);
        return Result.success();
    }

    @WebLog("修改用户")
    @ApiOperation("修改用户")
    @PostMapping("modifyUser")
    public Result<Void> modifyUser(@RequestBody UserDTO user) {
        userService.updateUser(user);
        return Result.success();
    }

    @WebLog("获取用户")
    @ApiOperation("获取用户")
    @GetMapping("getUser")
    public Result<UserDTO> getUser(@RequestParam("id") Long id) {
        return new Result<>(userService.getUserById(id));
    }

}
