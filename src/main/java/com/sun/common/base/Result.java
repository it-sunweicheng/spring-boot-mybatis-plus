package com.sun.common.base;

import com.google.gson.Gson;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 全局返回结果
 * 
 * @author sun
 */
@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -1L;

    @ApiModelProperty("返回结果")
    private Boolean           success          = false;

    @ApiModelProperty("错误编码")
    private String            errorCode        = "";

    @ApiModelProperty("错误内容")
    private String            errorMsg         = "";

    @ApiModelProperty("返回数据")
    private T                 value;

    public Result() {
        super();
    }

    public Result(boolean success) {
        super();
        this.success = success;
    }

    public Result(T value) {
        super();
        this.success = true;
        this.value = value;
    }

    public Result(String errorCode, String errorMsg) {
        super();
        this.success = false;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public Result(boolean success, String errorCode, String errorMsg) {
        super();
        this.success = success;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public static Result<Void> success() {
        Result<Void> result = new Result<>();
        result.setSuccess(true);
        return result;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
