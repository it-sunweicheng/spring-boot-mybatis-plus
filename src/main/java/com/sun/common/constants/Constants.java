package com.sun.common.constants;

/**
 * 通用常量
 * 
 * @author sun
 */
public class Constants {

    /** utf-8格式 */
    public static final String UTF_8_CODE          = "utf-8";
    /** gbk格式 */
    public static final String GBK_CODE            = "gbk";
    /** ISO-8859-1格式 */
    public static final String ISO_8859_1_CODE     = "ISO-8859-1";

    /** 通用时间格式年月日时分秒 */
    public static final String DATA_FORMAT_YMD_HMS = "yyyy-MM-dd HH:mm:ss";
    /** 通用时间格式年月日 */
    public static final String DATA_FORMAT_YMD     = "yyyy-MM-dd";

    /** 系统默认 */
    public static final String SYSTEM             = "system";

    /** N：标识正常 Y：标识逻辑删除 */
    public static final String IS_DELETED_Y        = "Y";
    public static final String IS_DELETED_N        = "N";

}
