package com.sun.common.exception;

import com.sun.common.base.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理
 * 
 * @author sun
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandle<T> {

    /**
     * Exception
     */
    @ExceptionHandler(Exception.class)
    public Result<T> resolveException(Exception e) {
        log.error(e.getMessage(), e);
        return new Result<T>("-1", "系统发生异常!");
    }

}
