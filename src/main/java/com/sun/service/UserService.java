package com.sun.service;

import com.sun.dto.UserDTO;

/**
 * 用户接口
 * @author sun
 */
public interface UserService {

    /**
     * 新增用户
     * @param user
     */
    void insertUser(UserDTO user);

    /**
     * 删除用户
     * @param id
     */
    void deleteUser(Long id);

    /**
     * 修改用户
     * @param user
     */
    void updateUser(UserDTO user);

    /**
     * 获取用户
     * @param id
     * @return
     */
    UserDTO getUserById(Long id);

}
