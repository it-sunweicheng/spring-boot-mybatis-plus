package com.sun.service;

import com.sun.dto.UserDTO;
import com.sun.entity.User;
import com.sun.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author sun
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    @Override
    @SneakyThrows
    public void insertUser(UserDTO userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        userMapper.insert(user);
    }

    @Override
    @SneakyThrows
    public void deleteUser(Long id) {
        userMapper.deleteById(User.builder().id(id).build());
    }

    @Override
    @SneakyThrows
    public void updateUser(UserDTO userDto) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setModifyTime(LocalDateTime.now());
        userMapper.updateById(user);
    }

    @Override
    public UserDTO getUserById(Long id) {
        UserDTO userDto = new UserDTO();
        User user = userMapper.selectById(id);
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

}
