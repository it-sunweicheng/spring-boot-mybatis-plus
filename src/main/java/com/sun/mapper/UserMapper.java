package com.sun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sun.entity.User;

/**
 * 通用Mapper
 * @author sun
 */
public interface UserMapper extends BaseMapper<User> {
}
